package models

import "time"

var GroupInput []Group

type Group struct {
	GroupName   string
	CreatedDate time.Time
	UpdatedDate *time.Time
}

func (group Group) InputGroup() {
	GroupInput = append(GroupInput, group)
}
