package models

import "time"

var UserInput []User

type User struct {
	UserName    string
	Email       string
	Role        Role
	Group       Group
	CreatedDate time.Time
	UpdatedDate *time.Time
}

func (user User) InputUser() {
	UserInput = append(UserInput, user)
}