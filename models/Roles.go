package models

import "time"

var RoleInput []Role

type Role struct {
	RoleName    string
	CreatedDate time.Time
	UpdatedDate *time.Time
}

func (role Role) InputRole() {
	RoleInput = append(RoleInput, role)
}