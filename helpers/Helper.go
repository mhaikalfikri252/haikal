package helpers

import (
	"bufio"
	"os"
	"strings"
)


func InArray(arr []string, findValue string) bool {
	found := false
	for i := 0; i < len(arr); i++ {
		if arr[i] == findValue {
			found = true
			break
		}
	}
	return found
}

func InputMenu() string {
	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	print("Pilih Menu[1-4]: ")
	menu, _ := reader.ReadString('\n')
	menu = strings.TrimSpace(menu)
	return menu
}