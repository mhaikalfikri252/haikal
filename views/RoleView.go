package views

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"haikal/models"
	"haikal/services"
)

func InputRoles() string {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	print("Role Name: ")
	rolename, _ := reader.ReadString('\n')
	rolename = strings.TrimSpace(rolename)

	return rolename

}


func IsUnique(rolename string) int {

	index := -1
	for i := 0; i < len(models.RoleInput); i++ {

		role := models.RoleInput[i]

		if role.RoleName == rolename {

			index = i
			break
		}
	}

	return index
}


func TambahRole() {

	loop := true

	for loop {

		rolename := InputRoles()

		uniqueIndex := IsUnique(rolename)
		if uniqueIndex > -1 {
			fmt.Println("Data yang Anda input sudah ada")
		} else {
			fmt.Println("1. Save")
			fmt.Println("2. Cancel")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				services.InputRole(rolename)
				loop = false
			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}

	}

}


func InputCariRoleByNama() string {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	fmt.Println("=========================================")
	print("Cari Role Name: ")
	role, _ := reader.ReadString('\n')
	role = strings.TrimSpace(role)

	return role

}


func CariRoleByNama(rolename string) (models.Role, int, bool) {

	found := false
	foundRole := models.Role{}
	foundIndex := -1

	for i := 0; i < len(models.RoleInput); i++ {
		if models.RoleInput[i].RoleName == rolename {
			found = true
			foundRole = models.RoleInput[i]
			foundIndex = i
			break
		}
	}

	return foundRole, foundIndex, found
}


func CariIndexRoleByNama(rolename string) int {
	foundIndex := -1
	for i := 0; i < len(models.RoleInput); i++ {
		if models.RoleInput[i].RoleName == rolename {
			foundIndex = i
			break
		}
	}

	return foundIndex
}


func UpdateRole() {

	loop := true

	for loop {

		nama := InputCariRoleByNama()

		index := CariIndexRoleByNama(nama)
		if index < 0 {

			fmt.Println("Data tidak ketemu")

		} else {

			fmt.Println("======================")
			fmt.Println("Input Update Role Name")
			fmt.Println("======================")
			rolename := InputRoles()

			uniqueIndex := IsUnique(rolename)


			if uniqueIndex > -1 {
				if uniqueIndex != index {
					fmt.Println("Data sudah ada ada/duplikat dari data lain")
				} else {
					services.UpdateDataRole(index, rolename)
					loop = false
				}
			}  else {
				

				fmt.Println("1. Save")
				fmt.Println("2. Cancel")
				var reader *bufio.Reader
				reader = bufio.NewReader(os.Stdin)
				print("Input: ")
				input, _ := reader.ReadString('\n')
				input = strings.TrimSpace(input)

				if input == "1" {
					services.UpdateDataRole(index, rolename)
					loop = false
				} else if input == "2" {
					loop = false
				} else {
					fmt.Println("Angka yang Anda input tidak tersedia")
					loop = true
				}
			}
		}
	}
}


func DeleteRole() {

	loop := true

	for loop {
		nama := InputCariRoleByNama()

		index := CariIndexRoleByNama(nama)
		if index < 0 {
			fmt.Println("Data RoleName tidak ada")

		} else {
			fmt.Println("1. Confirm Delete ?")
			fmt.Println("2. Back to List Role")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				services.DeleteData(index)
				loop = false
			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}
	}
}


func CariRole() {

	log.Println("Cari Role:")
	nama := InputCariRoleByNama()

	index := CariIndexRoleByNama(nama)

	if index < 0 {
		fmt.Println("RoleName yang Anda input tidak ada")
	} else {

		role := models.RoleInput[index]

		fmt.Println("============================================================================")
		fmt.Println("Cari Role")
		fmt.Println("============================================================================")
		fmt.Println("Role          |          Created At          |          Updated At          ")
		fmt.Println("============================================================================")

		fmt.Print(role.RoleName, " ", "|")
		fmt.Print(role.CreatedDate, " ", "|")
		fmt.Print(role.UpdatedDate, " ", "|")

		var reader *bufio.Reader
		reader = bufio.NewReader(os.Stdin)
		reader.ReadString('\n')

	}
}
