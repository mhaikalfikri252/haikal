package views

import (
	"bufio"
	"fmt"
	"haikal/models"
	"haikal/services"
	"log"
	"os"
	"strings"
)


func InputUsers() (string, string, string, string) {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)

	print("User Name: ")
	username, _ := reader.ReadString('\n')
	username = strings.TrimSpace(username)

	print("Email: ")
	email, _ := reader.ReadString('\n')
	email = strings.TrimSpace(email)

	print("Role: ")
	role, _ := reader.ReadString('\n')
	role = strings.TrimSpace(role)

	print("Group: ")
	group, _ := reader.ReadString('\n')
	group = strings.TrimSpace(group)

	return username, email, role, group

}


func IsUniqueUser(email string) int {

	index := -1
	for i := 0; i < len(models.UserInput); i++ {

		user := models.UserInput[i]

		if user.Email == email {
			index = i
			break
		}
	}
	return index

}


func TambahUser() {

	loop := true

	for loop {

		username, email, role, group := InputUsers()

		uniqueIndex := IsUniqueUser(email)
		if uniqueIndex > -1 {
			fmt.Println("Data yang Anda input sudah ada")
		} else {
			fmt.Println("1. Save")
			fmt.Println("2. Cancel")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				rolename := services.FindRole(role)
				groupname := services.FindGroup(group)
				if rolename == nil && groupname == nil {
					fmt.Println("Role atau Group tidak ketemu")
					loop = true
				} else {
					services.InputUser(username, email, role, group)
					loop = false
				}

			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}
	}
}


func InputCariUserByEmail() string {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	fmt.Println("====================================================================")
	print("Email: ")
	email, _ := reader.ReadString('\n')
	email = strings.TrimSpace(email)

	return email

}


func CariUserByEmail(email string) (models.User, int, bool) {

	found := false
	foundRole := models.User{}
	foundIndex := -1

	for i := 0; i < len(models.UserInput); i++ {
		if models.UserInput[i].Email == email {
			found = true
			foundRole = models.UserInput[i]
			foundIndex = i
			break
		}
	}

	return foundRole, foundIndex, found

}


func CariIndexUserByEmail(email string) int {
	foundIndex := -1
	for i := 0; i < len(models.UserInput); i++ {
		if models.UserInput[i].Email == email {
			foundIndex = i
			break
		}
	}
	return foundIndex
}


func UpdateUser() {

	loop := true

	for loop {

		email := InputCariUserByEmail()

		index := CariIndexUserByEmail(email)
		if index < 0 {

			fmt.Println("Email yang Anda input tidak ada")

		} else {

			fmt.Println("======================")
			fmt.Println("Input Update User Data")
			fmt.Println("======================")
			username, email, role, group := InputUsers()

			uniqueIndex := IsUniqueUser(email)

			if uniqueIndex > -1 {
				if uniqueIndex != index {
					fmt.Println("Data sudah ada ada/duplikat pada tempat lain")
				} else {
					services.UpdateDataUser(index, username, email, role, group)
					loop = false
				}
			} else {
				fmt.Println("1. Save")
				fmt.Println("2. Cancel")
				var reader *bufio.Reader
				reader = bufio.NewReader(os.Stdin)
				print("Input: ")
				input, _ := reader.ReadString('\n')
				input = strings.TrimSpace(input)

				if input == "1" {
					rolename := services.FindRole(role)
					groupname := services.FindGroup(group)

					if rolename == nil && groupname == nil {
						fmt.Println("Role atau Group tidak ditemukan")
					} else {
						services.UpdateDataUser(index, username, email, role, group)
						loop = false
					}
				} else if input == "2" {
					loop = false
				} else {
					fmt.Println("Angka yang Anda input tidak tersedia")
					loop = true
				}
			}
		}
	}
}


func DeleteUser() {

	loop := true

	for loop {

		email := InputCariUserByEmail()

		index := CariIndexUserByEmail(email)
		if index < 0 {
			fmt.Println("Data User tidak ada")

		} else {
			fmt.Println("1. Confirm Delete ?")
			fmt.Println("2. Back to List Users")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				services.DeleteDataUser(index)
				loop = false
			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}
	}
}


func CariUser() {

	log.Println("Cari User:")
	email := InputCariUserByEmail()

	index := CariIndexUserByEmail(email)

	if index < 0 {
		fmt.Println("Data tidak ketemu")

	} else {

		user := models.UserInput[index]

		fmt.Println("================================================================================")
		fmt.Println("Cari User")
		fmt.Println("================================================================================")
		fmt.Println("UserName   |   Email   |   Role   |   Group   |   Created At   |   Updated At   ")
		fmt.Println("================================================================================")

		fmt.Print(user.UserName, " ", "|")
		fmt.Print(user.Email, " ", "|")
		fmt.Print(user.Role.RoleName, " ", "|")
		fmt.Print(user.Group.GroupName, " ", "|")
		fmt.Print(user.CreatedDate, " ", "|")
		fmt.Print(user.UpdatedDate, " ", "|")

		var reader *bufio.Reader
		reader = bufio.NewReader(os.Stdin)
		reader.ReadString('\n')

	}
}
