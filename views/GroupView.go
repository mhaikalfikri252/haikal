package views

import (
	"bufio"
	"fmt"
	"haikal/models"
	"haikal/services"
	"log"
	"os"
	"strings"
)


func InputGroups() string {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	print("Group Name: ")
	groupname, _ := reader.ReadString('\n')
	groupname = strings.TrimSpace(groupname)

	return groupname

}


func IsUniqueGroup(groupname string) int {

	index := -1
	for i := 0; i < len(models.GroupInput); i++ {

		group := models.GroupInput[i]

		if group.GroupName == groupname {

			index = i
			break
		}
	}

	return index
}


func TambahGroup() {

	loop := true

	for loop {

		groupname := InputGroups()

		uniqueIndex := IsUniqueGroup(groupname)
		if uniqueIndex > -1 {
			fmt.Println("Data yang Anda input sudah ada")
		} else {
			fmt.Println("1. Save")
			fmt.Println("2. Cancel")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				services.InputGroup(groupname)
				loop = false
			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}
	}
}


func InputCariGroupByNama() string {

	var reader *bufio.Reader
	reader = bufio.NewReader(os.Stdin)
	fmt.Println("=========================================")
	print("Cari Group Name: ")
	group, _ := reader.ReadString('\n')
	group = strings.TrimSpace(group)

	return group
}


func CariGroupByNama(groupname string) (models.Group, int, bool) {

	found := false
	foundGroup := models.Group{}
	foundIndex := -1

	for i := 0; i < len(models.GroupInput); i++ {
		if models.GroupInput[i].GroupName == groupname {
			found = true
			foundGroup = models.GroupInput[i]
			foundIndex = i
			break
		}
	}

	return foundGroup, foundIndex, found
}


func CariIndexGroupByNama(groupname string) int {
	foundIndex := -1
	for i := 0; i < len(models.GroupInput); i++ {
		if models.GroupInput[i].GroupName == groupname {
			foundIndex = i
			break
		}
	}

	return foundIndex
}


func UpdateGroup() {

	loop := true

	for loop {

		nama := InputCariGroupByNama()

		index := CariIndexGroupByNama(nama)
		if index < 0 {

			fmt.Println("Data tidak ketemu")

		} else {

			fmt.Println("======================")
			fmt.Println("Input Update Group Name")
			fmt.Println("======================")
			groupname := InputGroups()

			uniqueIndex := IsUniqueGroup(groupname)

			if uniqueIndex > -1 {
				if uniqueIndex != index {
					fmt.Println("Data sudah ada ada/duplikat dari data lain")
				} else {
					services.UpdateDataGroup(index, groupname)
					loop = false
				}
			} else {

				fmt.Println("1. Save")
				fmt.Println("2. Cancel")
				var reader *bufio.Reader
				reader = bufio.NewReader(os.Stdin)
				print("Input: ")
				input, _ := reader.ReadString('\n')
				input = strings.TrimSpace(input)

				if input == "1" {
					services.UpdateDataGroup(index, groupname)
					loop = false
				} else if input == "2" {
					loop = false
				} else {
					fmt.Println("Angka yang Anda input tidak tersedia")
					loop = true
				}
			}
		}
	}
}


func DeleteGroup() {

	loop := true

	for loop {
		nama := InputCariGroupByNama()

		index := CariIndexGroupByNama(nama)
		if index < 0 {
			fmt.Println("Data Group Name tidak ada")

		} else {
			fmt.Println("1. Confirm Delete ?")
			fmt.Println("2. Back to List Group")
			var reader *bufio.Reader
			reader = bufio.NewReader(os.Stdin)
			print("Input: ")
			input, _ := reader.ReadString('\n')
			input = strings.TrimSpace(input)

			if input == "1" {
				services.DeleteDataGroup(index)
				loop = false
			} else if input == "2" {
				loop = false
			} else {
				fmt.Println("Angka yang Anda input tidak tersedia")
				loop = true
			}
		}
	}
}


func CariGroup() {

	log.Println("Cari Group:")
	nama := InputCariGroupByNama()

	index := CariIndexGroupByNama(nama)

	if index < 0 {
		fmt.Println("Group Name yang Anda input tidak ada")
	} else {

		group := models.GroupInput[index]

		fmt.Println("=============================================================================")
		fmt.Println("Cari Group Name")
		fmt.Println("=============================================================================")
		fmt.Println("Group          |          Created At          |          Updated At          ")
		fmt.Println("=============================================================================")

		fmt.Print(group.GroupName, " ", "|")
		fmt.Print(group.CreatedDate, " ", "|")
		fmt.Print(group.UpdatedDate, " ", "|")

		var reader *bufio.Reader
		reader = bufio.NewReader(os.Stdin)
		reader.ReadString('\n')

	}
}
