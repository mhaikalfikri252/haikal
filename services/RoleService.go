package services

import (
	"haikal/models"
	"time"
)

var RoleInput []models.Role

func InputRole(
	rolename string,
) {

	role := models.Role{}

	location, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}
	
	role.RoleName = rolename
	role.CreatedDate = time.Now().UTC().In(location)

	role.InputRole()
}


func FindRole(roleName string) *models.Role {
	index := -1
	for i := 0; i < len(models.RoleInput); i++ {
		if models.RoleInput[i].RoleName == roleName {
			index = i
			break
		}
	}

	if index > -1 {
		return &models.RoleInput[index]
	} else {
		return nil
	}
	
}


func UpdateDataRole(
	index int,
	rolename string,
) {

	for i := 0; i < len(models.RoleInput); i++ {
		if i == index {
			models.RoleInput[i].RoleName = rolename

			location, err := time.LoadLocation("Asia/Jakarta")
			if err != nil {
				panic(err)
			}

			updatedate := time.Now().UTC().In(location)
			models.RoleInput[i].UpdatedDate = &updatedate
			break
		}
	}
}


func DeleteData(index int) {
	models.RoleInput = append(models.RoleInput[:index], models.RoleInput[index+1:]...)
}
