package services

import (
	"time"
	"haikal/models"
)

var GroupInput []models.Group

func InputGroup(
	groupname string,
) {

	group := models.Group{}

	location, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}

	group.GroupName = groupname
	group.CreatedDate = time.Now().UTC().In(location)

	group.InputGroup()
}


func FindGroup(GroupName string) *models.Group {
	index := -1
	for i := 0; i < len(models.GroupInput); i++ {
		if models.GroupInput[i].GroupName == GroupName {
			index = i
			break
		}
	}

	if index > -1 {
		return &models.GroupInput[index]
	} else {
		return nil
	}
	
}


func UpdateDataGroup(
	index int,
	groupname string,
) {

	for i := 0; i < len(models.GroupInput); i++ {
		if i == index {
			models.GroupInput[i].GroupName = groupname

			location, err := time.LoadLocation("Asia/Jakarta")
			if err != nil {
				panic(err)
			}

			updatedate := time.Now().UTC().In(location)
			models.GroupInput[i].UpdatedDate = &updatedate
			break
		}
	}
}


func DeleteDataGroup(index int) {
	models.GroupInput = append(models.GroupInput[:index], models.GroupInput[index+1:]...)
}
