package services

import (
	"fmt"
	"haikal/models"
	"time"
)

var UserInput []models.User


func InputUser(
	username string,
	email string,
	rolename string,
	groupname string,
) {

	loop := true

	for loop {
		role := FindRole(rolename)
		if role == nil {
			// return nil, errors.New("Role tidak ditemukan")
			fmt.Println("Role tidak ditemukan")
			loop = true
		}

		group := FindGroup(groupname)
		if group == nil {
			// return nil, errors.New("Group tidak ditemukan")
			fmt.Println("Group tidak ditemukan")
			loop = true
		}

			user := models.User{}

			location, err := time.LoadLocation("Asia/Jakarta")
			if err != nil {
				panic(err)
			}
	
			user.UserName = username
			user.Email = email
			user.Role = *role
			user.Group = *group
			user.CreatedDate = time.Now().UTC().In(location)
			user.InputUser()

			loop = false
		}
	}


func UpdateDataUser(
	index int,
	username string,
	email string,
	rolename string,
	groupname string,
) {

	loop := true

	for loop {
		for i := 0; i < len(models.UserInput); i++ {
			if i == index {
	
				role := FindRole(rolename)
				group := FindGroup(groupname)
	
				if role == nil && group == nil {
					fmt.Println("Role atau Group tidak ditemukan, Silahkan input ulang data user")
					loop = true
				} else {
					location, err := time.LoadLocation("Asia/Jakarta")
					if err != nil {
						panic(err)
					}
		
					updateddate := time.Now().UTC().In(location)
		
					models.UserInput[i].UserName = username
					models.UserInput[i].Email = email
					models.UserInput[i].Role = *role
					models.UserInput[i].Group = *group
					models.UserInput[i].UpdatedDate = &updateddate
					loop = false
				}
			}
		}
	}
}


func DeleteDataUser(index int) {
	models.UserInput = append(models.UserInput[:index], models.UserInput[index+1:]...)
}
