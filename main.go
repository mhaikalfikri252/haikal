package main

import (
	"bufio"
	"fmt"
	"haikal/layouts"
	"os"
	"strings"
)


func main() {
	
	loop := true

	for loop {
		fmt.Println("============================")
		fmt.Println("User Management")
		fmt.Println("============================")
		fmt.Println("1. CRUD Role")
		fmt.Println("2. CRUD Group")
		fmt.Println("3. CRUD User")
		fmt.Println("4. Exit")
		fmt.Print("Input: ")
		
		var reader *bufio.Reader
		reader = bufio.NewReader(os.Stdin)

		input, _ := reader.ReadString('\n')
		input = strings.TrimSpace(input)

		switch true {
		case input == "1":
			layouts.RoleMenu()
		case input == "2":
			layouts.GroupMenu()
		case input == "3":
			layouts.UserMenu()
		case input == "4":
			loop = false
		}
	}

}
