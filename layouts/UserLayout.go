package layouts

import (
	"fmt"
	"haikal/views"
	"haikal/models"
	"haikal/helpers"
)


func listUser(users []models.User) {
	fmt.Println("===========================================================================================")
	fmt.Println("List User")
	fmt.Println("===========================================================================================")
	fmt.Println("UserName    |    Email    |    Role    |    Group    |    Created At    |    Updated At    ")
	fmt.Println("===========================================================================================")

	printRowsListUser(users)

}

func printRowsListUser(users []models.User) {
	for i := 0; i < len(users); i++ {
		user := users[i]
		fmt.Println(user.UserName, "|", user.Email, "|", user.Role.RoleName, "|" , user.Group.GroupName, "|", user.CreatedDate, "|", user.UpdatedDate)
	}
}

func printMenuUser() {
	fmt.Println("1. Tambah User")
	fmt.Println("2. Update User")
	fmt.Println("3. Delete User")
	fmt.Println("4. Back to Menu")
}

func menuUser() string {
	menu := ""
	loop := true

	for loop {
		printMenuUser()

		menu = helpers.InputMenu()

		actions := []string{
			"1",
			"2",
			"3",
			"4",
		}

		if helpers.InArray(actions, menu) {
			loop = false
		} else {
			fmt.Println("Hanya ada pilihan dari angka 1 sampai 4!")
		}

	}
	return menu
}

func UserMenu() {
	loop := true

	for loop {
		listUser(models.UserInput)

		input := menuUser()

		switch true {
		case input == "1":
			views.TambahUser()
		case input == "2":
			views.UpdateUser()
		case input == "3":
			views.DeleteUser()
		case input == "4":
			loop = false
		}
	}
}