package layouts

import (
	"fmt"
	"haikal/views"
	"haikal/models"
	"haikal/helpers"
)


func listRole(roles []models.Role) {
	fmt.Println("============================================================================")
	fmt.Println("List Role")
	fmt.Println("============================================================================")
	fmt.Println("Role          |          Created At          |          Updated At          ")
	fmt.Println("============================================================================")

	printRowsListRole(roles)

}

func printRowsListRole(roles []models.Role) {
	for i := 0; i < len(roles); i++ {
		role := roles[i]
		fmt.Println(role.RoleName, "|", role.CreatedDate, "|", role.UpdatedDate)
	}
}

func printMenuRole() {
	fmt.Println("1. Tambah Role")
	fmt.Println("2. Update Role")
	fmt.Println("3. Delete Role")
	fmt.Println("4. Back to Menu")
}

func menuRole() string {
	menu := ""
	loop := true

	for loop {
		printMenuRole()

		menu = helpers.InputMenu()

		actions := []string{
			"1",
			"2",
			"3",
			"4",
		}

		if helpers.InArray(actions, menu) {
			loop = false
		} else {
			fmt.Println("Hanya ada pilihan dari angka 1 sampai 4!")
		}

	}
	return menu
}

func RoleMenu() {
	loop := true

	for loop {
		listRole(models.RoleInput)

		input := menuRole()

		switch true {
		case input == "1":
			views.TambahRole()
		case input == "2":
			views.UpdateRole()
		case input == "3":
			views.DeleteRole()
		case input == "4":
			loop = false
		}
	}
}