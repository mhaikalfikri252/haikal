package layouts

import (
	"fmt"
	"haikal/views"
	"haikal/models"
	"haikal/helpers"
)

func listGroup(groups []models.Group) {
	fmt.Println("==================================================================================")
	fmt.Println("List Group")
	fmt.Println("==================================================================================")
	fmt.Println("Group Name          |          Created At          |          Updated At          ")
	fmt.Println("==================================================================================")

	printRowsListGroup(groups)
	
}

func printRowsListGroup(groups []models.Group) {
	for i := 0; i < len(groups); i++ {
		group := groups[i]
		fmt.Println(group.GroupName, "|", group.CreatedDate, "|", group.UpdatedDate)
	}
}

func printMenuGroup() {
	fmt.Println("1. Tambah Group")
	fmt.Println("2. Update Group")
	fmt.Println("3. Delete Group")
	fmt.Println("4. Back to Menu")
}

func menuGroup() string {
	menu := ""
	loop := true

	for loop {
		printMenuGroup()

		menu = helpers.InputMenu()

		actions := []string{
			"1",
			"2",
			"3",
			"4",
		}

		if helpers.InArray(actions, menu) {
			loop = false
		} else {
			fmt.Println("Hanya ada pilihan dari angka 1 sampai 4!")
		}

	}
	return menu
}

func GroupMenu() {
	loop := true

	for loop {
		listGroup(models.GroupInput)

		input := menuGroup()

		switch true {
		case input == "1":
			views.TambahGroup()
		case input == "2":
			views.UpdateGroup()
		case input == "3":
			views.DeleteGroup()
		case input == "4":
			loop = false
		}
	}
}